package net.therap.logParser;

import net.therap.logParser.controller.LogController;

/**
 * @author mohammad.hossain
 * @since 12/20/21
 */
public class LogParser {

    public static void main(String[] args) {
        LogController.init(args);
    }
}
