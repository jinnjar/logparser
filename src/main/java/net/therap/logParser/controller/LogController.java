package net.therap.logParser.controller;

import net.therap.logParser.model.Log;
import net.therap.logParser.model.LogSummary;
import net.therap.logParser.service.LogService;
import net.therap.logParser.view.LogView;

import java.util.Collections;
import java.util.List;

/**
 * @author mohammad.hossain
 * @since 12/20/21
 */
public class LogController {

    public static void init(String... args) {
        if (args.length < 1) {
            System.out.println("Wrong command line arguments...");
            System.out.println("Exiting...");
            return;
        }

        LogService logService = new LogService();
        LogView logView = new LogView();
        boolean sort = args.length == 2 && "--sort".equals(args[1]);

        List<Log> logList = logService.logInit(args[0]);
        List<LogSummary> logSummaries = logService.logSummaryInit(logList);

        if (sort) {
            Collections.sort(logSummaries);
        }

        logView.printSummary(logSummaries);
    }
}
