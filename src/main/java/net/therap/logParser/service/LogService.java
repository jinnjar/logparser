package net.therap.logParser.service;

import net.therap.logParser.model.Log;
import net.therap.logParser.model.LogSummary;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author mohammad.hossain
 * @since 12/20/21
 */
public class LogService {

    private static final int DAYS = 1;

    public List<Log> logInit(String file) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd H:mm:ss,SSS");
        List<Log> logList = new ArrayList<>();
        List<String> logInfo;
        String content;
        Log log;

        try {
            Scanner sc = new Scanner(new File(file));

            while (sc.hasNext()) {
                if ((content = sc.nextLine()).contains("URI")) {
                    logInfo = Arrays.stream(content.split(" "))
                            .collect(Collectors.toList());

                    // Building log class
                    log = buildLog(logInfo, formatter);
                    logList.add(log);
                }
            }

            // To keep only 24 hours log data
            cleanLog(logList);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return logList;
    }

    public List<LogSummary> logSummaryInit(List<Log> logList) {
        List<LogSummary> logSummaries = new ArrayList<>();
        List<List<Log>> groupedLogs = new ArrayList<>();
        List<Log> groupedLog = new ArrayList<>();
        LogSummary logSummary;

        // Group log data hourly
        groupLogHourly(logList, groupedLogs, groupedLog);

        for (List<Log> groupedLogList : groupedLogs) {
            logSummary = buildLogSummary(groupedLogList);
            logSummaries.add(logSummary);
        }

        return logSummaries;
    }

    private Log buildLog(List<String> logInfo, DateTimeFormatter formatter) {
        return Log.builder()
                .dateTime(LocalDateTime.parse(logInfo.get(0) + " " + logInfo.get(1), formatter))
                .requestType(logInfo.contains("G,") ? "GET" : "POST")
                .uri(logInfo.stream()
                        .filter(s -> s.contains("URI"))
                        .findFirst()
                        .orElse("").replace("URI", "")
                        .replaceAll("[=\\[\\],]", ""))
                .responseTime(Integer.parseInt(logInfo.get(logInfo.size() - 1)
                        .replace("time=", "")
                        .replace("ms", "")))
                .build();
    }

    private void cleanLog(List<Log> logList) {
        Collections.sort(logList);
        LocalDateTime initDate = logList.get(0).getDateTime();

        for (int i = 1; i < logList.size(); i++) {
            if (ChronoUnit.DAYS.between(initDate, logList.get(i).getDateTime()) > LogService.DAYS) {
                logList.subList(i, logList.size() - 1).clear();
                break;
            }
        }
    }

    private void groupLogHourly(List<Log> logList, List<List<Log>> groupedLogs, List<Log> groupedLog) {
        LocalDateTime startTime = logList.get(0).getDateTime().withMinute(0).withSecond(0).withNano(0);
        LocalDateTime endTime = logList.get(0).getDateTime().plusHours(1).withMinute(0).withSecond(0).withNano(0);
        for (int i = 0; i < logList.size() - 1; i++) {
            if (logList.get(i).getDateTime().isBefore(endTime) && logList.get(i).getDateTime().isAfter(startTime)) {
                groupedLog.add(logList.get(i));
            } else {
                groupedLogs.add(groupedLog);

                groupedLog = new ArrayList<>();
                groupedLog.add(logList.get(i));

                startTime = logList.get(i).getDateTime().withMinute(0).withSecond(0).withNano(0);
                endTime = logList.get(i).getDateTime().plusHours(1).withMinute(0).withSecond(0).withNano(0);
            }
        }
        groupedLogs.add(groupedLog);
    }

    private LogSummary buildLogSummary(List<Log> groupedLogList) {
        return LogSummary.builder()
                .startTime(groupedLogList.get(0).getDateTime())
                .endTime(groupedLogList.get(groupedLogList.size() - 1).getDateTime())
                .getCount(groupedLogList.stream()
                        .filter(log -> "GET".equals(log.getRequestType()))
                        .count())
                .postCount(groupedLogList.stream()
                        .filter(log -> "POST".equals(log.getRequestType()))
                        .count())
                .uniqueUriCount(groupedLogList.stream()
                        .map(Log::getUri)
                        .distinct()
                        .count())
                .totalResponseTime(groupedLogList.stream()
                        .mapToLong(Log::getResponseTime)
                        .sum())
                .build();
    }
}
