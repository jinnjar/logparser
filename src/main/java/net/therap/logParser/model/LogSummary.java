package net.therap.logParser.model;

import lombok.*;

import java.time.LocalDateTime;

/**
 * @author mohammad.hossain
 * @since 12/20/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogSummary implements Comparable<LogSummary> {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Long getCount;
    private Long postCount;
    private Long uniqueUriCount;
    private Long totalResponseTime;

    @Override
    public int compareTo(LogSummary o) {
        return Long.compare(o.getGetCount() + o.getPostCount(), this.getGetCount() + this.getPostCount());
    }
}
