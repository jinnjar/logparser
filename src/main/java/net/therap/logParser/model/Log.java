package net.therap.logParser.model;

import lombok.*;

import java.time.LocalDateTime;

/**
 * @author mohammad.hossain
 * @since 12/20/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Log implements Comparable<Log> {

    private LocalDateTime dateTime;
    private String requestType;
    private String uri;
    private Integer responseTime;

    @Override
    public int compareTo(Log log) {
        return this.getDateTime().compareTo(log.getDateTime());
    }
}
