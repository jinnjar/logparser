package net.therap.logParser.view;

import net.therap.logParser.model.LogSummary;

import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author mohammad.hossain
 * @since 12/20/21
 */
public class LogView {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH.mm a");

    public void printSummary(List<LogSummary> logSummaries) {
        System.out.println("-------------------------------------------------------------------------------------" +
                "---------------------------------------");
        System.out.printf("%10s %35s %35s %40s\n", "Time", "GET/POST Count",
                "Unique URI Count", "Total Response Time");
        System.out.println("-------------------------------------------------------------------------------------" +
                "---------------------------------------");

        for (LogSummary logSummary : logSummaries) {

            System.out.printf("%s %21s %33s %40s\n",
                    logSummary.getStartTime().withMinute(0).format(formatter) + " - "
                            + logSummary.getEndTime().withMinute(0).plusHours(1).format(formatter),
                    logSummary.getGetCount() + "/" + logSummary.getPostCount(),
                    logSummary.getUniqueUriCount(),
                    logSummary.getTotalResponseTime() + "ms");
        }
    }
}
